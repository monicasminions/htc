<html>
	<head>
		<title> 	Fast Admin Page 	</title>
		<style>
			.button {
				background-color: #4CAF50; /* Green */
				border: none;
				color: white;
				padding: 15px 41px;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				font-size: 16px;
			}
			
			button:active {
				border: 2px solid green;
			}
			button:target {
				border: 2px solid red;
			}
			
			.textinput{
				background-color: #4CAF50; /* Green */
				border: none;
				color: white;
				padding: 15px 32px;
				text-align: center;
				font-size: 16px;
			}
			
			.endbutton {
				background-color: #f06600;
				border: none;
				color: white;
				padding: 15px 32px;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				font-size: 16px;
			}
					
			endbutton:active {
				border: 2px solid green;
			}
			endbutton:target {
				border: 2px solid red;
			}
			
		</style>
	</head>
	<?php
		require_once('htc_register.php');
		$stubdb = new HTC_DB();
		if($_POST){
			if (isset($_POST['state'])) {
				$state = $_POST["state"];
				updateState($state);
			}
		}
		function updateState($state){
			$db = new HTC_DB();
			$db->changeState($state);
		}
	?>
	<body>
		<div id="start_emergency_mode">
			<form method="post" action="http://padc.orangefix.net/htc/admin.php">
				<input type="hidden" name="state" value="EMERGENCY">
				<button type="submit" class="button"> Activate Disaster Mode </button> 
				<input type="text" id="event_name" class="textinput"/>
			</form>
		</div>
		<div id="end_emergency_mode">
			<hr>
			<br>
			<form method="post" action="http://padc.orangefix.net/htc/admin.php">
				<input type="hidden" name="state" value="NORMAL">
				<button type="submit" class="endbutton"> Deactivate Disaster Mode </button> 
			</form>
			<br>
		</div>
		<div id="Game Time Respondents">
			<hr>
			<table>
				<th>
					<td> Mobile Number </td>
					<td> Location </td>
				</th>
				<?php
					$data = $stubdb->getEmergencies();
					foreach($data as $row){
						echo "<tr>";
						echo "<td>".$row['mobile']."</td>";
						echo "<td>".$row['viewer_loc']."</td>";
						echo "<td>".$row['status']."</td>";
						echo "</tr>";
					}
				?>
			</table>
		</div>
	</body>
</html>