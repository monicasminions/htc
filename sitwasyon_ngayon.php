<?php
	require_once ('htc_register.php');
	$lat = $_POST["lat"];
	$long= $_POST["long"];

	$stubdb = new HTC_DB();
	$status = $stubdb->getcurrentstate();
	
	
	function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo) {
		$earthRadius = 6371000;
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);

		$latDelta = $latTo - $latFrom;
		$lonDelta = $lonTo - $lonFrom;

		$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
		cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
		return ($angle * $earthRadius ) / 1000;
	}
	
	$distance = distance($status['storm_pt_1_lat'], $status['storm_pt_1_long'], $lat, $long, "K");
	
	if($distance <= 30){
		$typhoon_danger = 4; //red
		$speil = "tatama yung bagyo dyan... i suggest pack up and leave! love you po. please take care. ill check with you again later";
		
		if(($status['windspeed'] >= 100) || $status['typhoon_signal'] == 3){
			$wind_danger = 4; //red
			$speil.= "malakas ang hangin ng bagyo maaaring makaranas ng mga lumilipad na mga bagay. Please stay indoors. Wag na mag pumilit lumabas.";
		}else if(($status['windspeed'] >= 30 && $status['windspeed'] < 60)  || $status['typhoon_signal'] == 1){
			$speil.= "Mahangin sa labas Hmmm... pero sa tingin ko makinig ka na ng balita.";
			$wind_danger = 2; //yellow
		}else if(($status['windspeed'] >= 60 && $status['windspeed'] < 100) || $status['typhoon_signal'] == 2){
			$speil.= "Galing ako sa labas medyo nakikita kong nag bebend na mga puno. Wag ka ng mag balak lumabas.";
			$wind_danger = 3; //orange
		}
	
	}else if($distance > 30 && $distance <= 60){
		$typhoon_danger = 3; //orange
		$speil = "malapit ka sa bagyo... i suggest pack up na! ill check with you again later";
		
		if(($status['windspeed'] >= 100) || $status['typhoon_signal'] == 3){
			$wind_danger = 4; //red
			$speil.= "malakas ang hangin ng bagyo maaaring makaranas ng mga lumilipad na mga bagay. Please stay indoors. Wag na mag pumilit lumabas.";
		}else if(($status['windspeed'] >= 30 && $status['windspeed'] < 60)  || $status['typhoon_signal'] == 1){
			$speil.= "Mahangin sa labas Hmmm... pero sa tingin ko makinig ka na ng balita.";
			$wind_danger = 2; //yellow
		}else if(($status['windspeed'] >= 60 && $status['windspeed'] < 100) || $status['typhoon_signal'] == 2){
			$speil.= "Galing ako sa labas medyo nakikita kong nag bebend na mga puno. Wag ka ng mag balak lumabas.";
			$wind_danger = 3; //orange
		}
	}else if($distance > 60 && $distance <= 90){
		$typhoon_danger = 2; //yellow
		$speil = "mag masid ng maiigi malapit ka sa bagyo.";
		if(($status['windspeed'] >= 100) || $status['typhoon_signal'] == 3){
			$wind_danger = 4; //red
			$speil.= "malakas ang hangin ng bagyo maaaring. Sa tingin ko mas ok mag stay ka muna sa bahay.";
		}else if(($status['windspeed'] >= 30 && $status['windspeed'] < 60)  || $status['typhoon_signal'] == 1){
			$speil.= "Mahangin sa labas Hmmm... sa tingin ko nararapat lang na mag pahinga ka na lang muna sa bahay sakt ang hangin malamig.";
			$wind_danger = 2; //yellow
		}else if(($status['windspeed'] >= 60 && $status['windspeed'] < 100) || $status['typhoon_signal'] == 2){
			$speil.= "Malakas lakas  ang hangin sa tingin ko naman mag handa ka na";
			$wind_danger = 3; //orange
		}
	}else if($distance > 120){
		$typhoon_danger = 1; //green
		$speil = "Malayo ka sa bagyo. Please be vigilant na lang.";
		$speil.= "Hindi mo mararanas ang hagupit ng hangin ng bagyo.";
	}
	
	if($status['antecedent_rainfall'] < 100){
		$water_danger = 1; //green
		$speil.= "walang naiipong tubig na mula sa bagyo";
	}else if($status['antecedent_rainfall'] >= 100 && $status['antecedent_rainfall'] < 150){
		$speil.= "Yuck! nabasa sapatos ko. may mga puddles of water na...";
		$water_danger = 2; //yellow
	}else if($status['antecedent_rainfall'] >= 150 && $status['antecedent_rainfall'] < 200){
		$speil.= "OMG! OMG! I saw the water it was upto my knee! naku! Please prepare na to evcuate";
		$water_danger = 3; //orange
	}else if($status['antecedent_rainfall'] >= 200){
		$speil.= "NO! NO! NO! this cant be please immediately move out. Punta ka na ng designation safe zone";
		$water_danger = 4; //red
	}
	
	if($water_danger == 4 || $water_danger == 3){
		$speil.= "Mag ingat na po tayo sa mga ilog at katawang tubig baka po pumasok na ng bahay";
		$speil.= "Kung nasa tabi po tayo ng bundok, maaari po tayong mag matyag upang makita natin yung ";
	}
	
	$arr = array('status' => " ", 'message' => " ", 'distance' => " ");

	//if - else 
	$arr['status'] = "ALERt Mode";
	$arr['message'] = $speil; 
	$arr['distance'] = $distance;
	$arr[lat] = $status['storm_pt_1_lat'];
	$arr[long] = $status['storm_pt_1_long'];
	
    echo json_encode($arr);
?>