<?php 
		class HTC_DB{
			private $conn = null;
			private $hostname = "localhost";
			private $username = "htc_user";
			private $password = "htc_pass_A_1234";
			
			function insertUser($mobile, $name, $bday, $gender, $address){
						
				$conn = mysql_connect($this->hostname, $this->username, $this->password) 
					or die("Unable to connect to MySQL");
				$error = false;
				
				//mobile number PK
				$sql = "INSERT INTO viewers "." (mobile , name, bday, gender, address) "." VALUES "." ('".$mobile."','".$name."','".$bday."', '".$gender."', '".$address."')" ;

				mysql_select_db('padc_htc_db');
				$retval = mysql_query( $sql, $conn );
				
				if(! $retval ){
					//die('Could not enter data: ' . mysql_error());
					$error = true;
				}
				mysql_close($conn);
				return $error;
			}

			function getUsers(){
				$conn = mysql_connect($this->hostname, $this->username, $this->password) 
					or die("Unable to connect to MySQL");
				$error = false;
				
				if(mysqli_connect_errno()){
					$viewers = array();
				}else{
					$row_data = array('mobile' => " ", 'name' => " ", 'bday' => " ", 'gender' => " ", 'address' => " ");
					$viewers = array();
			
					$sql = "SELECT mobile, name, bday, gender, address FROM viewers" ;
					mysql_select_db('padc_htc_db');
					$result = mysql_query($conn, $sql);

					if (mysqli_num_rows($result) > 0) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							$row_data['mobile'] = $row["mobile"];
							$row_data['name'] = $row["name"];
							$row_data['bday'] = $row["bday"];
							$row_data['gender'] = $row["gender"];
							$row_data['address'] = $row["address"];
							array_push($viewers, $row_data);
							}
					}
					mysqli_free_result($result);
					mysqli_close($conn);
				}
				return $viewers;
			}
		
			function insertUserEmergency($mobile, $lat, $long, $status){
				$conn = mysql_connect($this->hostname, $this->username, $this->password) 
					or die("Unable to connect to MySQL");
				$error = false;
				
				//mobile number PK
				$sql = "INSERT INTO viewer_emergency (mobile , viewer_lat, viewer_long, status)  VALUES  ('".$mobile."','".$lat."','".$long."', '".$status."')" ;

				mysql_select_db('padc_htc_db');
				$retval = mysql_query( $sql, $conn );
				
				if(! $retval ){
					//die('Could not enter data: ' . mysql_error());
					$error = true;
				}
				mysql_close($conn);
				return $error;
			}
		
			function getEmergencies(){
				$conn = new mysqli($this->hostname, $this->username, $this->password, 'padc_htc_db') 
					or die("Unable to connect to MySQL");
				$error = false;
				
				$row_data = array('mobile' => " ", 'viewer_lat' => " ", 'viewer_long' => " ", 'status' => " ");

				$sql = "SELECT mobile, viewer_lat, viewer_long, status FROM viewer_emergency" ;
				
				$result = $conn->query($sql);
				$viewers = array();
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()){
						$row_data['mobile'] = $row["mobile"];
						$row_data['viewer_loc'] = "(".$row["viewer_lat"].",".$row["viewer_long"].")";
						$row_data['status'] = $row["status"];
						array_push($viewers, $row_data);
					}
				}
				mysql_close($conn);
				return $viewers;
			}
			
			function getcurrentstate(){
				$conn = new mysqli($this->hostname, $this->username, $this->password, 'padc_htc_db') 
					or die("Unable to connect to MySQL");
				$error = false;
				
				$row_data = array('storm_pt_1_lat' => " ", 'storm_pt_1_long' => " ", 'storm_pt_2_lat' => " ", 'storm_pt_2_long' => " ", 'storm_pt_3_lat' => " ", 'storm_pt_3_long' => " ", 'storm_pt_4_lat' => " ", 'storm_pt_4_long' => " ", 'windspeed' => " ", 'typhoon_signal' => " ", 'antecedent_rainfall' => " ");

				$sql = "SELECT storm_pt_1_lat, storm_pt_1_long, storm_pt_2_lat, ";
				$sql.= " storm_pt_2_long, storm_pt_3_lat, storm_pt_3_long, storm_pt_4_lat, storm_pt_4_long, ";
				$sql.= " windspeed, typhoon_signal, antecedent_rainfall FROM emergency_state" ;
				
				$result = $conn->query($sql);
				
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()){
						$row_data['storm_pt_1_lat'] = $row["storm_pt_1_lat"];
						$row_data['storm_pt_1_long'] = $row["storm_pt_1_long"];
						$row_data['storm_pt_2_lat'] = $row["storm_pt_2_lat"];
						$row_data['storm_pt_2_long'] = $row["storm_pt_2_long"];
						$row_data['storm_pt_3_lat'] = $row["storm_pt_3_lat"];
						$row_data['storm_pt_3_long'] = $row["storm_pt_3_long"];
						$row_data['storm_pt_4_lat'] = $row["storm_pt_4_lat"];
						$row_data['storm_pt_4_long'] = $row["storm_pt_4_long"];
						$row_data['windspeed'] = $row["windspeed"];
						$row_data['typhoon_signal'] = $row["typhoon_signal"];
						$row_data['antecedent_rainfall'] = $row["antecedent_rainfall"];
					}
				}
				mysql_close($conn);
				return $row_data;
			}
			
			function insertGameVotes($mobile){
						
				$conn = mysql_connect($this->hostname, $this->username, $this->password) 
					or die("Unable to connect to MySQL");
				$error = false;
				
				//mobile number PK
				$sql = "INSERT INTO viewers_votes "." (mobile) "." VALUES "." ('".$mobile."')" ;

				mysql_select_db('padc_htc_db');
				$retval = mysql_query( $sql, $conn );
				
				if(! $retval ){
					//die('Could not enter data: ' . mysql_error());
					$error = true;
				}
				
				mysql_close($conn);
				return $error;
			}
			
			function changeState($value){
				$conn = mysql_connect($this->hostname, $this->username, $this->password) 
					or die("Unable to connect to MySQL");
				$error = false;
				
				//mobile number PK
				$sql = "INSERT INTO current_state "." (state) "." VALUES "." ('".$value."')" ;

				mysql_select_db('padc_htc_db');
				$retval = mysql_query( $sql, $conn );
				
				if(! $retval ){
					//die('Could not enter data: ' . mysql_error());
					$error = true;
				}
				
				mysql_close($conn);
				return $error;
			}
			
			function getState(){
				$conn = new mysqli($this->hostname, $this->username, $this->password, 'padc_htc_db') 
					or die("Unable to connect to MySQL");
				$error = false;
				
				$sql = "SELECT state FROM current_state ORDER BY timestamp DESC LIMIT 1";				
				$result = $conn->query($sql);
				
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()){
						$general_state = $row["state"];
					}
				}
				mysql_close($conn);
				return $general_state;
			}
		}
	?>