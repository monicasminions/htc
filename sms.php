<?php
	require_once('htc_register.php');
	
	function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo) {
		$earthRadius = 6371000;
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);
		$latDelta = $latTo - $latFrom;
		$lonDelta = $lonTo - $lonFrom;
		$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
		cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
		return ($angle * $earthRadius ) / 1000;
	}
	
    try{
        $type = $_POST["message_type"];
    }catch (Exception $e){
        echo "Error 1 : ".$e;
        exit(0);
    }
	
    if (strtoupper($type) == "INCOMING"){
        try{
            $message = $_POST["message"];
            $mobile_number = $_POST["mobile_number"];
            $shortcode = $_POST["shortcode"];
            $timestamp = $_POST["timestamp"];
            $request_id = $_POST["request_id"];
			$response = "SPEIL";
			$stubdb = new HTC_DB();
			$part = substr( $message, 0, 5 );
	
			if(strcmp( $part,'games') == 0){
				$stubdb->insertGameVotes($mobile_number);
				$response = "Salamat po sa pagsali sa aming laro. May the force be with you";
			}else if(strcmp( $part, 'state') == 0){
				$stubdb = new HTC_DB();
				$status1 = $stubdb->getcurrentstate();
				$data_array = explode("|", $message);
				$lat = $data_array[1];
				$long = $data_array[2];
				
				$distance = distance($status1['storm_pt_1_lat'], $status1['storm_pt_1_long'], $lat, $long, "K");
				
				if($distance <= 30){
					$typhoon_danger = 4; //red
					$speil = "tatama yung bagyo dyan... i suggest pack up and leave! love you po. please take care. ill check with you again later";
					
					if(($status['windspeed'] >= 100) || $status['typhoon_signal'] == 3){
						$wind_danger = 4; //red
						$speil.= "malakas ang hangin ng bagyo maaaring makaranas ng mga lumilipad na mga bagay. Please stay indoors. Wag na mag pumilit lumabas.";
					}else if(($status['windspeed'] >= 30 && $status['windspeed'] < 60)  || $status['typhoon_signal'] == 1){
						$speil.= "Mahangin sa labas Hmmm... pero sa tingin ko makinig ka na ng balita.";
						$wind_danger = 2; //yellow
					}else if(($status['windspeed'] >= 60 && $status['windspeed'] < 100) || $status['typhoon_signal'] == 2){
						$speil.= "Galing ako sa labas medyo nakikita kong nag bebend na mga puno. Wag ka ng mag balak lumabas.";
						$wind_danger = 3; //orange
					}
				
				}else if($distance > 30 && $distance <= 60){
					$typhoon_danger = 3; //orange
					$speil = "malapit ka sa bagyo... i suggest pack up na! ill check with you again later";
					
					if(($status['windspeed'] >= 100) || $status['typhoon_signal'] == 3){
						$wind_danger = 4; //red
						$speil.= "malakas ang hangin ng bagyo maaaring makaranas ng mga lumilipad na mga bagay. Please stay indoors. Wag na mag pumilit lumabas.";
					}else if(($status['windspeed'] >= 30 && $status['windspeed'] < 60)  || $status['typhoon_signal'] == 1){
						$speil.= "Mahangin sa labas Hmmm... pero sa tingin ko makinig ka na ng balita.";
						$wind_danger = 2; //yellow
					}else if(($status['windspeed'] >= 60 && $status['windspeed'] < 100) || $status['typhoon_signal'] == 2){
						$speil.= "Galing ako sa labas medyo nakikita kong nag bebend na mga puno. Wag ka ng mag balak lumabas.";
						$wind_danger = 3; //orange
					}
				}else if($distance > 60 && $distance <= 90){
					$typhoon_danger = 2; //yellow
					$speil = "mag masid ng maiigi malapit ka sa bagyo.";
					if(($status['windspeed'] >= 100) || $status['typhoon_signal'] == 3){
						$wind_danger = 4; //red
						$speil.= "malakas ang hangin ng bagyo maaaring. Sa tingin ko mas ok mag stay ka muna sa bahay.";
					}else if(($status['windspeed'] >= 30 && $status['windspeed'] < 60)  || $status['typhoon_signal'] == 1){
						$speil.= "Mahangin sa labas Hmmm... sa tingin ko nararapat lang na mag pahinga ka na lang muna sa bahay sakt ang hangin malamig.";
						$wind_danger = 2; //yellow
					}else if(($status['windspeed'] >= 60 && $status['windspeed'] < 100) || $status['typhoon_signal'] == 2){
						$speil.= "Malakas lakas  ang hangin sa tingin ko naman mag handa ka na";
						$wind_danger = 3; //orange
					}
				}else if($distance > 120){
					$typhoon_danger = 1; //green
					$speil = "Malayo ka sa bagyo. Please be vigilant na lang.";
					$speil.= "Hindi mo mararanas ang hagupit ng hangin ng bagyo.";
				}
				
				if($status['antecedent_rainfall'] < 100){
					$water_danger = 1; //green
					$speil.= "walang naiipong tubig na mula sa bagyo";
				}else if($status['antecedent_rainfall'] >= 100 && $status['antecedent_rainfall'] < 150){
					$speil.= "Yuck! nabasa sapatos ko. may mga puddles of water na...";
					$water_danger = 2; //yellow
				}else if($status['antecedent_rainfall'] >= 150 && $status['antecedent_rainfall'] < 200){
					$speil.= "OMG! OMG! I saw the water it was upto my knee! naku! Please prepare na to evcuate";
					$water_danger = 3; //orange
				}else if($status['antecedent_rainfall'] >= 200){
					$speil.= "NO! NO! NO! this cant be please immediately move out. Punta ka na ng designation safe zone";
					$water_danger = 4; //red
				}
				
				if($water_danger == 4 || $water_danger == 3){
					$speil.= "Mag ingat na po tayo sa mga ilog at katawang tubig baka po pumasok na ng bahay";
					$speil.= "Kung nasa tabi po tayo ng bundok, maaari po tayong mag matyag upang makita natin yung ";
				}
				$response = $speil; 
			}else if(strcmp( $part, 'helps') == 0){
				$data_array = explode("|", $message);
				$lat = $data_array[1];
				$long = $data_array[2];
				$status = $data_array[3];
				
				$stubdb->insertUserEmergency($mobile_number	, $lat, $long, $status);
				$response = "Nakuha na po namin yung mensahe ninyo. Please keep safe until we can reach you.";
			}
			
			//generate message id
			//"12345678901234567890123456789012"
			$randoma = rand(10000, 99999);
			$randomb = rand(10000, 99999);
			$randomc = rand(10000, 99999);
			$randomd = rand(10, 99);
			
			if($randoma%3 == 0){
				$messageid = $randoma."12345".$randomb."67890".$randomc."13579".$randomd;
			}else if($randoma%3 == 1){
				$messageid = $randomd.$randoma."12345".$randomb."67890".$randomc."24680";
			}else{
				$messageid = $randoma."12345".$randomb.$randomd."67890".$randomc."08642";
			}
			
			//sending sms
			$arr_post_body = array(
				"message_type" => "SEND",
				"mobile_number" => $mobile_number,
				"shortcode" => "2929027693",
				"message_id" => $messageid,
				"message" => urlencode("SMS Recieved.".$response),
				"request_id	"=> $request_id,
				"client_id" => "9147ae428f491cb6876548f93f8e5fe8162f9a3e260c9dd469d31f7e41e6d46b",
				"secret_key" => "33b127999d1cd02f395b8693c74d1998ef2fed4b5df1c49d3db5ec0207d1b228",
				"request_cost" => "P1.00"
			);
			$query_string = "";
			foreach($arr_post_body as $key => $frow){
				$query_string .= '&'.$key.'='.$frow;
			}
			//save sending sms
			echo "\nSENDING SMS: ".$query_string;
			$URL = "https://post.chikka.com/smsapi/request";
			$curl_handler = curl_init();
			curl_setopt($curl_handler, CURLOPT_URL, $URL);
			curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
			curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
			curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
			$response = curl_exec($curl_handler);
			curl_close($curl_handler);
            exit(0);
        }catch (Exception $e){
            echo "Error 2 : ".$e;
            exit(0);
        }
    }else{
        echo "Error 3 : ".$type;
        exit(0);
    }
?>